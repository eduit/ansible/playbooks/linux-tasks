# Global Default Settings

Settings here may be overwritten in other `group_vars` files.
Overwriting values in several specific `group_vars` files may have unintended effects.

Duplicate overwrites in exclusive `group_vars` like _Umgebung_ (production, testing etc.) ARE allowed: only
one _Umgebung_ group can be active for a host, therefore only one groupwide variable
will be set.
