Group Vars Organization
=======================

For more general groups, subdirectory are used. Groups like *Production* will accumulate many differents settings. 
Having them all in one file `Production.yml` will get messy. They rather go to files in a subdirectory `Production`.
Up to here, case matters! SnipeIt uses upper case in the *Service* property, therefore service groups are upper case.

The file names in such a group subdirectory have no particular meaning, nor any case rules. All files
in the subdirectory are evaluated. It helps to name them according to their intended use:

```
group_vars
  +-- all.yml
  +-- Moodle.yml
  +-+ Production
    +-- mariadb.yml
	+-- moodle.yml
```

Remember that a variable may be defined in `all.yml`, and **only one** specific group vars file.
Variables in `all.yml` are overwritten by i.e. variables in `Production/mariadb.yml`. With a 
variable also defined in `Moodle.yml`, the outcome is undefined.
