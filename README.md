# Ansible Playbooks for EduIT Linux

This README describes the concepts and usage of the global `site.yml`.

## Main Playbook
The `site.yml` is the main playbook for service configurations. It can be applied to any
host registered in *SnipeIT*.

It works best with the inventory by *SnipeIT*. In *Semaphore*, use the *SnipeIt full live inventory* and the environment *SnipeIT*.

### Base Settings
Base settings like SSH key management, motd etc. can be applied to any host. Hosts installed with the boot server environment automatically apply these settings and ensure that hosts can be accessed by SSH.

### Specific Roles
For each specific host role, a dedicated playbook is imported. All hosts import all role playbooks, host filtering occurs in the playbooks.

With dedicated playbooks, it is possible to apply a specific role without passing all the base settings, which reduces the time to run.

## Specific Role Playbooks
### General
Role playbooks have a prefix `application_` and must filter hosts:
```yaml
- name: DP-Portal DB setup
  hosts: DP_Portal:&backend
  roles:
    - let.services.postgresql
```
[https://docs.ansible.com/ansible/latest/inventory_guide/intro_patterns.html](Ansible filter patterns documented).

### Contents
As usual with playbooks, they may contain a list of tasks. Tasks may apply roles or tasks:
```
- name: SEB-Server setup
  hosts: SEB_Server:&backend
  roles:
    - let.services.mariadb

- name: Select hosts for Nagios plugins
  hosts: SEB_Server:!sebdev
  tasks:
    - name: Build dynamic group
      ansible.builtin.group_by:
        key: seb_nagios
      when: inventory_hostname_short in groups.Production or inventory_hostname_short in groups.Staging or inventory_hostname_short in groups.Testing
      changed_when: false
      
- name: Deploy Nagios plugins
  hosts: seb_nagios
  roles:
    - let.base_settings.nagios_plugins

- name: Developer access to MariaDB dev
  hosts: SEB_Server:&Development:&backend
  tasks:
    - name: Open firewall for all (in fact VPZ 'let')
      ansible.posix.firewalld:
        rich_rule: "rule family=ipv4 source address=129.132.0.0/16 service name=mysql accept"
        permanent: true
        immediate: true
        state: enabled
        zone: "{{ firewall_zone }}"
```

If tasks are of a general kind, consider to move them to a collection role.

## Role Settings Guidelines
Apart from the defaults in the service role, here are the preferred places for configurations. All files are relative to the playbook using the role,
usually in the root of the [Linux Tasks](https://gitlab.ethz.ch/eduit/ansible/playbooks/linux-tasks) repo.

### All EduIT Installations
To overwrite service role default for all EduIT setups, put them into `group_vars/all/<file>.yml`. Definitions in more
specific group variables overwrite these variables.

### Overwrites for Development, Testing, Staging, Production
These groups are exclusive in *SnipeIT*, only one can be defined for a host. Variables in these `group_vars` files
therefore do not interfere. It is legal to define the same variables here.

Example: the activation for DB backups for service *mariadb* can be set
- in `group_vars/Development` to **true**
- in `group_vars/Production` to **false**

### Overwrites for Generic Services
Variable files for generic (reused) services like
- let.services.mariadb
- let.services.postgresql
- let.services.firewall

should be used with caution, since they can only be overwritten in `host_vars`.

### Configurations for Specific Services
Variable files for use with specific services
- let.services.SEB_Server
- let.services.Otter_Grader

contain settings valid for all instances, like common certificates, databases, or enabling SELinux.

### Configuration for Hosts
As little as possible should go to host specific configuration. Good examples are
- users and passwords (local or databases)
- DB backup activation for dev or test
- program versions
